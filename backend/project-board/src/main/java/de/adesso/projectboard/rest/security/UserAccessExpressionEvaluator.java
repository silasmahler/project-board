package de.adesso.projectboard.rest.security;

import de.adesso.projectboard.base.access.service.UserAccessService;
import de.adesso.projectboard.base.application.service.ApplicationService;
import de.adesso.projectboard.base.project.persistence.Project;
import de.adesso.projectboard.base.project.service.ProjectService;
import de.adesso.projectboard.base.security.ExpressionEvaluator;
import de.adesso.projectboard.base.user.persistence.User;
import de.adesso.projectboard.base.user.persistence.data.UserData;
import de.adesso.projectboard.base.user.service.UserProjectService;
import de.adesso.projectboard.base.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

/**
 * A {@link ExpressionEvaluator} implementation that is used to authorize access
 * to the REST interface by using persisted user data.
 *
 * <p>
 *     Activated via the <i>user-access</i> spring profile.
 * </p>
 *
 * @see ExpressionEvaluator
 */
@Profile("user-access")
@Service
public class UserAccessExpressionEvaluator implements ExpressionEvaluator {

    private final UserService userService;

    private final UserAccessService userAccessService;

    private final ProjectService projectService;

    private final UserProjectService userProjectService;

    private final ApplicationService applicationService;

    @Autowired
    public UserAccessExpressionEvaluator(UserService userService,
                                         UserAccessService userAccessService,
                                         ProjectService projectService,
                                         UserProjectService userProjectService,
                                         ApplicationService applicationService) {
        this.userService = userService;
        this.userAccessService = userAccessService;
        this.projectService = projectService;
        this.userProjectService = userProjectService;
        this.applicationService = applicationService;
    }

    /**
     *
     * @param authentication
     *          The {@link Authentication} object.
     *
     * @param user
     *          The {@link User} object of the currently authenticated user.
     *
     * @return
     *          {@code true}, iff the user is a manager or has a active
     *          access info instance.
     *
     * @see UserAccessService#userHasActiveAccessInfo(User)
     * @see UserService#userIsManager(User)
     */
    @Override
    public boolean hasAccessToProjects(Authentication authentication, User user) {
        return userAccessService.userHasActiveAccessInfo(user) || userService.userIsManager(user);
    }

    /**
     *
     * @param authentication
     *          The {@link Authentication} object.
     *
     * @param user
     *          The {@link User} object of the currently authenticated user.
     *
     * @param projectId
     *          The id of the {@link Project}
     *          the user wants to access.
     *
     * @return
     *          {@code true}, iff the no project with the given {@code projectId} exists,
     *          {@link ApplicationService#userHasAppliedForProject(User, Project)} returns {@code true},
     *          {@link UserProjectService#userOwnsProject(User, Project)} returns {@code true} or
     *          {@link #hasAccessToProjects(Authentication, User)} returns {@code true}
     *          and at least one of the following conditions is true:
     *
     *          <ul>
     *              <li>
     *                  The user is a manager and the project's status is set to <i>open</i> or <i>eskaliert</i>.
     *              </li>
     *
     *              <li>
     *                  The user is not a manager and the project's status is set to <i>eskaliert</i> or <i>open</i>
     *                  and the project's LoB is equal to the user's LoB.
     *              </li>
     *          </ul>
     *
     */
    @Override
    public boolean hasAccessToProject(Authentication authentication, User user, String projectId) {
        // return true if the project does not exist, the user owns the project or
        // the user has applied for the project
        if(!projectService.projectExists(projectId) ||
                userProjectService.userOwnsProject(user, projectService.getProjectById(projectId)) ||
                applicationService.userHasAppliedForProject(user, projectService.getProjectById(projectId))) {

            return true;
        }

        Project project = projectService.getProjectById(projectId);

        if(hasAccessToProjects(authentication, user)) {
            UserData userData = userService.getUserData(user);

            boolean isManager = userService.userIsManager(user);
            boolean isOpen = "open".equalsIgnoreCase(project.getStatus());
            boolean isEscalated = "eskaliert".equalsIgnoreCase(project.getStatus());
            boolean sameLobAsUser = userData.getLob().equalsIgnoreCase(project.getLob());
            boolean noLob = project.getLob() == null;

            // escalated || isOpen <-> (sameLob || noLob)
            // equivalence because implication is not enough
            // when the status is neither "eskaliert" nor "open"
            return isEscalated || (isOpen && isManager) || ((isOpen && (sameLobAsUser || noLob)) || (!isOpen && !(sameLobAsUser || noLob)));
        }

        return false;
    }

    /**
     *
     * @param authentication
     *          The {@link Authentication} object.
     *
     * @param user
     *          The {@link User} object of the currently authenticated user.
     *
     * @return
     *          The result of {@link #hasAccessToProjects(Authentication, User)}
     *
     * @see #hasAccessToProjects(Authentication, User)
     */
    @Override
    public boolean hasPermissionToApply(Authentication authentication, User user) {
        return hasAccessToProjects(authentication, user);
    }

    /**
     *
     * @param authentication
     *          The {@link Authentication} object.
     *
     * @param user
     *          The {@link User} object of the currently authenticated user.
     *
     * @param userId
     *          The id of the {@link User} the current user wants to access.
     *
     * @return
     *          {@code true}, when the currently authenticated user has the same {@link User#getId() id}
     *          or the result of {@link #hasElevatedAccessToUser(Authentication, User, String)}.
     */
    @Override
    public boolean hasPermissionToAccessUser(Authentication authentication, User user, String userId) {
        return user.getId().equals(userId) || hasElevatedAccessToUser(authentication, user, userId);
    }

    /**
     *
     * @param authentication
     *          The {@link Authentication} object.
     *
     * @param user
     *          The {@link User} object of the currently authenticated user.
     *
     * @return
     *          {@code true}, if the given {@link User} is a manager,
     *          {@code false} otherwise.
     */
    @Override
    public boolean hasPermissionToCreateProjects(Authentication authentication, User user) {
        return userService.userIsManager(user);
    }

    /**
     * A {@link User} has the permission to edit a {@link Project} when it is
     * present in the {@link User#ownedProjects owned projects} of the user.
     *
     * @param authentication
     *          The {@link Authentication} object.
     *
     * @param user
     *          The {@link User} object of the currently authenticated user.
     *
     * @param projectId
     *          The id of the {@link Project} the user wants to update.
     *
     * @return
     *          The result of {@link UserProjectService#userOwnsProject(User, Project)}.
     */
    @Override
    public boolean hasPermissionToEditProject(Authentication authentication, User user, String projectId) {
        if(projectService.projectExists(projectId)) {
            return userProjectService.userOwnsProject(user, projectService.getProjectById(projectId));
        }

        return true;
    }

    /**
     *
     * @param authentication
     *          The {@link Authentication} object.
     *
     * @param user
     *          The {@link User} object of the currently authenticated user.
     *
     * @param userId
     *          The id of the {@link User}
     *          the current user wants to access.
     *
     * @return
     *          The result of {@link UserService#userHasStaffMember(User, User)} iff
     *          {@link UserService#userExists(String)} returns {@code true}, {@code true}
     *          otherwise.
     */
    @Override
    public boolean hasElevatedAccessToUser(Authentication authentication, User user, String userId) {
        if(userService.userExists(userId)) {
            return userService.userHasStaffMember(user, userService.getUserById(userId));
        }

        return true;
    }

}
