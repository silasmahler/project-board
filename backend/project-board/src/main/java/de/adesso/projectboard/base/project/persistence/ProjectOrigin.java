package de.adesso.projectboard.base.project.persistence;

/**
 * Enum to describe the origin of a {@link Project}.
 */
public enum ProjectOrigin {
    JIRA,
    CUSTOM
}
