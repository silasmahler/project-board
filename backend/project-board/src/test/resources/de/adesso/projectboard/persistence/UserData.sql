-- Requires Users.sql to be executed first

INSERT INTO PB_USER_DATA (ID, USER_ID, FIRST_NAME, LAST_NAME, EMAIL, LOB) VALUES
(1, 'User1', 'First', 'User', 'first.user@test.com', 'LOB Test'),
(2, 'User2', 'Second', 'User', 'second.user@test.com', 'LOB Test');